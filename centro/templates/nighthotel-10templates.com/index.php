<?php
/**
 * Date         February 02, 2012
 * Copyright    Copyright (C) 2012 10templates.com
 * License  GPL
 */
defined('_JEXEC') or die;
$app = JFactory::getApplication();
$leftbar = 1;
$rightbar = 1;
$needToWork = '<p style="font-size:10px; clear: both; text-align:right; padding-top: 10px;">Design by <a target="_blank" href="http://10templates.com">Premium templates</a> in association with <a target="_blank" href="http://templatefreejoomla.com">Free Joomla 2.5 templates</a><p>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />



<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/nivo-slider.css" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
<?php
if ($this->countModules('left') == 0)
	$leftbar	= "0";

if ($this->countModules('right') == 0)
	$rightbar	= "0";
?>
</head>

<body>
<div class="mainContainer">
<div class="header overHid">
			<?php
	  	if ($this->params->get('logoType') == 'image') { ?>
        
	  		<a href="<?php echo $this->baseurl ?>" class="logo" title="<?php echo $app->getCfg('sitename'); ?>" style="background: url(<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/logo.jpg) no-repeat;"></a>

	  	<?php } else { ?>
	  	<div class="logo-text">
	  		<p class="siteLogoText"><a href="<?php echo $this->baseurl ?>" title="<?php echo $this->params->get('logoText'); ?>"><span><?php echo $this->params->get('logoText'); ?></span></a></p>
	  		<p class="site-slogan"><?php echo $this->params->get('sloganText');?></p>
	  	</div>
        
	  	<?php } ?>
			<div class="bannerTop">          
            <a href="<?php echo $this->params->get('twitter-ic');?>" target="_blank" class="twic"></a>
            <a href="<?php echo $this->params->get('google-ic');?>" target="_blank" class="goic"></a>
            <a href="<?php echo $this->params->get('facebook-ic');?>" target="_blank" class="fbic"></a>
			</div>
            
		</div>
       
     <div class="nav2">
    <jdoc:include type="modules" name="top" style="xhtml" />
    <div class="clear"></div>
  	</div>          
    

<?php 
$option = JRequest::getCmd('option');
$view = JRequest::getCmd('view');
$frontpage = ($option == 'com_content' && $view == 'featured');
$article = ($option == 'com_content' && $view == 'article');
?>
<?php if ($frontpage): ?>

<div class="sliderPlace">
        <div id="slider" class="nivoSlider">
            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/bear.jpg" alt="" title="#htmlcaption" data-transition="slideInRight" />
            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/up.jpg" alt="" title="#htmlcaption2" />
            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/toystory.jpg" alt="" title="#htmlcaption3" data-transition="boxRainReverse" />
        </div>
        <div id="htmlcaption" class="nivo-html-caption">
            <?php echo $this->params->get('caption1');?>
        </div>
        <div id="htmlcaption2" class="nivo-html-caption">
            <?php echo $this->params->get('caption2');?>
        </div>
        <div id="htmlcaption3" class="nivo-html-caption">
            <?php echo $this->params->get('caption3');?>
        </div>
</div>
<h1><?php echo $this->params->get('welcome');?></h1>
<?php endif; ?>


<div class="overHid">
  
  <div class="leftCont">
  	<?php if($this->countModules('left')) : ?>
    <jdoc:include type="modules" name="left" style="xhtml" />
    <?php endif; ?>
    <div id="maincolumn<?php echo (2-$leftbar-$rightbar); ?>">
    <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
    <jdoc:include type="component" />
  </div>
  </div>
  
  
  <?php if($this->countModules('right')) : ?>
  <div class="rightCont">
    <jdoc:include type="modules" name="right" style="xhtml" />
  </div>
  <?php endif; ?>
  <div class="clear">
</div>

</div> 

    </div>
    <div class="footer">
    	<div class="colFoot">
        <?php if($this->countModules('bottom1')) : ?>
        	<jdoc:include type="modules" name="bottom1" style="xhtml" />
        <?php endif; ?>
        </div>
        <div class="colFoot">
        	<jdoc:include type="modules" name="bottom2" style="xhtml" />
        </div>
        <div class="colFoot">
        	<jdoc:include type="modules" name="bottom3" style="xhtml" />
        </div>
      <jdoc:include type="modules" name="footer" style="xhtml" />
      <?php echo $needToWork ?>
    </div>
    <jdoc:include type="modules" name="debug" />
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery-1.4.2.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/noconflict.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery.nivo.slider.js"></script>  
</body>
</html>