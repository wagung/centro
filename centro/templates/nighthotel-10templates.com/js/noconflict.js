//10templates.com JavaScript Document
jQuery.noConflict();

    jQuery(document).ready(function(){
		jQuery(window).load(function() {
        jQuery('#slider').nivoSlider();
    });
});

jQuery(function(){

    jQuery("ul.menu li").hover(function(){
    
        jQuery(this).addClass("hover");
        jQuery('ul:first',this).css('visibility', 'visible');
    
    }, function(){
    
        jQuery(this).removeClass("hover");
        jQuery('ul:first',this).css('visibility', 'hidden');
    
    });
    
    jQuery("ul.menu li ul li:has(ul)").find("a:first").append(" &raquo; ");

});