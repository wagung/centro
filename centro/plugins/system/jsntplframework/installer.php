<?php
/**
 * @version     $Id$
 * @package     JSNExtension
 * @subpackage  TPLFramework
 * @author      JoomlaShine Team <support@joomlashine.com>
 * @copyright   Copyright (C) 2012 JoomlaShine.com. All Rights Reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Class to implement hook for installation process
 *
 * @package     TPLFramework
 * @subpackage  Plugin
 * @since       1.0.0
 */
class PlgSystemJSNTPLFrameworkInstallerScript
{
	/**
	 * Implement postflight hook.
	 *
	 * @param   string  $route  Route type: install, update or uninstall.
	 * @param   object  $_this  The installer object.
	 *
	 * @return  boolean
	 */
	public function postflight($route, $_this)
	{
		// Get a database connector object
		$db = JFactory::getDbo();

		try
		{
			// Enable plugin by default
			$q = $db->getQuery(true);

			$q->update('#__extensions');
			$q->set(array('enabled = 1', 'protected = 1', 'ordering = 9999'));
			$q->where("element = 'jsntplframework'");
			$q->where("type = 'plugin'", 'AND');
			$q->where("folder = 'system'", 'AND');

			$db->setQuery($q);

			method_exists($db, 'execute') ? $db->execute() : $db->query();

			// Migrate custom v1 parameters specified in page class suffix to compatible with v2
			$db = JFactory::getDbo();
			$customizableColumns = array('promoleft', 'promoright', 'left', 'right', 'innerleft', 'innerright');

			$q = $db->getQuery(true);

			$q->select('id, params');
			$q->from('#__menu');
			$q->where('params LIKE "%custom-' . implode('width-%" OR params LIKE "%custom-', $customizableColumns) . 'width-%"');

			$db->setQuery($q);

			if ($rows = $db->loadObjectList())
			{
				foreach ($rows AS $row)
				{
					// Get all specified custom parameters
					if (preg_match_all('/custom-(' . implode('|', $customizableColumns) . ')width-([\d\.]+)/', $row->params, $matches, PREG_SET_ORDER))
					{
						foreach ($matches AS $match)
						{
							$row->params = str_replace($match[0], "custom-{$match[1]}width-span" . round($match[2] / (100 / 12)), $row->params);
						}

						// Update database record
						$q = $db->getQuery(true);

						$q->update('#__menu');
						$q->set("params = '" . $row->params . "'");
						$q->where('id = ' . (int) $row->id);

						$db->setQuery($q);
						method_exists($db, 'execute') ? $db->execute() : $db->query();
					}
				}
			}
		}
		catch (Exception $e)
		{
			throw $e;
		}
	}
}
